using UnityEngine;

public interface IRayCheck
{
  RaycastHit2D ForwardHitRayCheck();
  Collider2D ColliderAreaCheck();
}