public interface IMovement
{
  void MoveForward();
  void MoveBackward();
  void MoveStop();
}
