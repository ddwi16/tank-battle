public interface IRotation
{
  void RotateLeft();
  void RotateRight();
  void RotateBarrel();
}