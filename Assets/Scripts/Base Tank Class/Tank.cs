using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct TankProperty
{
  public const float HEALTH = 100;

  public const float MOV_SPEED = 50f;
  public const float ROTATION_VALUE = 90f;

  public const float BULLET_SPEED = 200f;
  public const float BULLET_DAMAGE = 10f;

  public const float RAY_DISTANCE = 0.2f;
  public const float AREA_DISTANCE = 1f;
}

public abstract class Tank : MonoBehaviour, IMovement, IRotation, IAttack, IRayCheck
{
  protected Rigidbody2D rb;
  protected RaycastHit2D rayForward;

  [Header("Controller")]
  [SerializeField]
  protected Transform rayPosition;
  [SerializeField]
  protected LayerMask wallMask;

  [Space]
  [Header("Attacking")]
  [SerializeField]
  protected GameObject bulletPrefab;
  [SerializeField]
  protected GameObject bulletPosition;

  protected bool isDetectWall;
  protected bool isDetectEnemy;

  protected bool isIdling;
  protected bool isMoving;
  protected bool isShooting;

  private void Start() => OnStartTank();
  private void FixedUpdate() => OnFixedUpdateTank();

  public virtual void OnStartTank() => rb = GetComponent<Rigidbody2D>();
  public virtual void OnFixedUpdateTank() => rayForward = ForwardHitRayCheck();

  #region TANK_MOVEMENT
  public virtual void MoveForward() => rb.velocity = transform.up * TankProperty.MOV_SPEED * Time.deltaTime;
  public virtual void MoveBackward() => rb.velocity = transform.up * -TankProperty.MOV_SPEED * Time.deltaTime;
  public virtual void MoveStop() => rb.velocity = Vector2.zero;
  #endregion

  #region TANK_ROTATION
  public virtual void RotateLeft() => transform.RotateAround(transform.position, transform.forward, TankProperty.ROTATION_VALUE);
  public virtual void RotateRight() => transform.RotateAround(transform.position, transform.forward, -TankProperty.ROTATION_VALUE);
  public void RotateBarrel()
  {
    throw new System.NotImplementedException();
  }
  #endregion

  #region  TANK_RAYCAST_EVENTS
  public RaycastHit2D ForwardHitRayCheck() => Physics2D.Raycast(rayPosition.transform.position, transform.up, TankProperty.RAY_DISTANCE, wallMask);
  public Collider2D ColliderAreaCheck() => Physics2D.OverlapCircle(transform.position, TankProperty.AREA_DISTANCE);
  #endregion

  #region TANK_ATTACK_EVENTS
  public virtual void CastBullet() => Instantiate(bulletPrefab, bulletPosition.transform.position, bulletPosition.transform.rotation);

  protected IEnumerator Shooting()
  {
    while (isShooting)
    {
      CastBullet();
      yield return new WaitForSeconds(3f);
    }
  }
  #endregion
}
