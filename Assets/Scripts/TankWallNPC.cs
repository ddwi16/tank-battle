using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWallNPC : Tank
{
  public override void OnStartTank()
  {
    base.OnStartTank();
    isShooting = true;
    StartCoroutine(Shooting());
  }

  public override void OnFixedUpdateTank()
  {
    base.OnFixedUpdateTank();

    MovementCheck(rayForward);

    if (isDetectWall)
    {
      RotateLeft();
      isDetectWall = false;
    }

    MoveForward();
  }

  private void MovementCheck(RaycastHit2D hitInfo)
  {
    if (hitInfo) isDetectWall = true;
    else isDetectWall = false;
  }
}
