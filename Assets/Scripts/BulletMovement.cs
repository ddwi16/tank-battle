using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour, IMovement
{
  Rigidbody2D bulletRb;

  public virtual void MoveForward() => bulletRb.velocity = transform.up * TankProperty.BULLET_SPEED * Time.deltaTime;
  public void MoveBackward() => throw new System.NotImplementedException();
  public void MoveStop() => throw new System.NotImplementedException();

  private void Start()
  {
    bulletRb = GetComponent<Rigidbody2D>();
    StartCoroutine(DestroyAfter());
  }

  private void FixedUpdate()
  {
    MoveForward();
  }

  IEnumerator DestroyAfter()
  {
    yield return new WaitForSeconds(2);
    Destroy(gameObject);
  }
}
